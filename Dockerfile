FROM node:8-slim as builder

MAINTAINER Davide Polonio <poloniodavide@gmail.com>
LABEL Telegram bot for the AC project

WORKDIR /usr/src/app/
RUN apt-get update && \
apt-get upgrade -y && \
apt-get install -y git yarn && \
git clone https://gitlab.com/acontroller/actgbot.git --depth=1 && \
cd actgbot/ && \
yarn install && \
yarn run build

FROM node:8-slim

WORKDIR /usr/src/app/
COPY --from=builder /usr/src/app/ .
RUN npm install --production
ENTRYPOINT ["npm", "start", "--production"]