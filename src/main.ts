import { TgMiddleware } from './tgMiddleware'
import databaseManager from './utils/databaseManager'

class Main {
    public static main (): void {

        console.log('Launching the bot...')

        if (!databaseManager.isInitialized()) {
            databaseManager.initialize()
        }

        new TgMiddleware()
        console.log('Bot launched, waiting for events')
    }
}

Main.main()
