import * as TelegramBot from 'node-telegram-bot-api'
import { ConfigChecker, IConfiguration } from './utils/configChecker'

export class TgMiddleware {

    private tgApi: TelegramBot

    public constructor () {

        console.log('In the constructor')

        ConfigChecker.getConfiguration().fold(
            (err: string) => {
                console.log('Error: ' + err)
                throw new Error(err)
            },
            (config: IConfiguration) => {
                this.tgApi = new TelegramBot(config.token, {polling: true})

                this.setRoute()
            }
        )
    }

    private setRoute (): void {
        this.tgApi.onText(/\/start/, (msg: TelegramBot.Message,
                                      match: RegExpExecArray) => {
            const chatId = msg.chat.id

            this.tgApi.sendMessage(chatId, 'Hi!')
                .then((value: TelegramBot.Message | Error) => {
                    console.log('Message successfully sent')
                })
                .catch((err) => {
                    console.log(err)
                })
        })
    }
}
