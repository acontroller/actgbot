import { ConfigChecker } from './configChecker'

class DatabaseManager {

    private url: string

    public constructor () {

        ConfigChecker.getConfiguration().fold(
            () => {throw new Error('Need a valid config file')},
            (json) => this.url = json.dbUrl
        )
    }

    public initialize (): void {
        this.reset()
    }

    public isInitialized (): boolean {
        return true
    }

    public reset (): void {

    }

    public getDbVersion (): number {

        return 42
    }
}
const databaseManager = new DatabaseManager()
export default databaseManager