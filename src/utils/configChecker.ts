import * as fs from 'fs'
import { Err, Ok, Option, Result, Some } from 'space-lift/'

export interface IConfiguration {
    token: string
    dbUrl: string
}

export class ConfigChecker {

    private static CONFIG_PATH: string = process.env.CONFIG_PATH || 'config.json'
    private static json: Option<IConfiguration> = Option<IConfiguration>(
        JSON.parse(fs.readFileSync(ConfigChecker.CONFIG_PATH, 'utf8'))
    )

    private static isValid (json: IConfiguration) {

        const isTokenValid = Some(json.token).fold(
            () => false,
            (_) => true
        )
        const isDbUrlValid = Some(json.dbUrl).fold(
            () => false,
            (_) => true
        )

        return isTokenValid && isDbUrlValid
    }

    public static getConfiguration (): Result<string, IConfiguration> {

        return this.json.fold(
            () => Err('Config not found. Expected in ' + ConfigChecker.CONFIG_PATH),
            (json) => {
                if (this.isValid(json)) {
                    return Ok({
                        token: json.token,
                        dbUrl: json.dbUrl
                    })
                } else {
                    return Err('The configuration is not valid')
                }
            }
        )
    }
}
